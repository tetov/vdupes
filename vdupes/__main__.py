import vobject
from pathlib import Path
import argparse
import os
from typing import Sequence, Mapping, List

def files_from_args(files : List[Path]) -> List[Path]:
    for file_ in files:
        path = Path(file_)
        if path.is_dir():
            for child in path.iterdir():
                if child.is_file():
                    yield child
        else:
            yield path


def main(files : Sequence[os.PathLike], excluded_fields : Sequence[str]=None) -> Mapping[Path, Path]:
    vobjects = [vobject.readOne(path.read_text()) for path in files_from_args(files)]

    for vobj in vobjects:
        for excluded_field in excluded_fields or []:
            try:
                delattr(vobj, excluded_field)
            except AttributeError:
                pass


    print(vobjects[0])

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Find duplicate vcards based on fields")
    parser.add_argument("files", type=Path, default=[Path(os.getcwd())], nargs="+" )
    parser.add_argument("-x", "--exclude-field", action="append", type=str)
    args = parser.parse_args()

    print(main(args.files, args.exclude_field))
